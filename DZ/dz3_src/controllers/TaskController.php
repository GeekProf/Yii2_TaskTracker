<?php

namespace app\controllers;

use app\models\behaviors\MyBehavior;
use app\models\tables\Task;
use app\models\tables\User;
use app\models\Test;
use yii\base\Event;
use yii\web\Controller;

class TaskController extends Controller
{
    public function actionIndex()
    {
        $userId = \Yii::$app->user->getId();
        $calendar = array_fill_keys(range(1, date("t")), []);

        foreach (Task::getByCurrentMonth($userId) as $task){
            $date = \DateTime::createFromFormat("Y-m-d H:i:s", $task->date);
            $calendar[$date->format("j")][] = $task;
        }

        return $this->render('index', ['calendar' => $calendar]);
    }

    public function actionCreate()
    {
        $model = new Task();
        if($model->load(\Yii::$app->request->post()) && $model->save()){
            $this->redirect(['task/index']);
        }
        return $this->render('create', ['model' => $model]);
    }

    public function actionTest()
    {

      /*  $handler =  function($e){var_dump($e); "обработчик сработал!!!";};
        Event::on(Test::class, Test::EVENT_RUN_START, $handler);*/


        $model = new Test();
        $model->attachBehavior('my', ['class' => MyBehavior::class, "message" => 'jdsghsdhfjjh']);
       // $model->detachBehavior('my');
        $model->foo();
        //$model->run();



        exit;
    }

}