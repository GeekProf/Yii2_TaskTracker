<?php
namespace app\models\behaviors;
use yii\base\Behavior;

class MyBehavior extends Behavior
{
    public $message;

    public function foo(){
        echo $this->message;
    }
}