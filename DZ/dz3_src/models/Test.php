<?php


namespace app\models;
use app\models\behaviors\MyBehavior;
use app\models\events\TestRunStartEvent;
use yii\base\Event;
use yii\base\Model;

class Test extends Model
{
    public $title;
    public $content;

    const EVENT_RUN_START = 'run_start';

    public function rules()
    {
        return [
          [['title'], 'required'],
          [['content'], 'safe'],
        ];
    }
    public function titleValidation($attribute)
    {
        if($this->$attribute != 'test'){
            $this->addError($attribute, "Неверно указан заголовок");
        }
    }
    public function fields()
    {
        return [
          'header' => 'title'
        ];
    }

   /* public function behaviors()
    {
       return [
           'my' =>[
               'class' => MyBehavior::class,
               'message' => "Привет, я дружелюбное поведение!!"
           ],
       ];
    }*/


    public function run()
    {
        echo "метод ран запущен<br>";
        echo "метод ран работает<br>";
        $this->trigger(static::EVENT_RUN_START, new TestRunStartEvent(['id' => 2]));
        echo "метод ран завершается<br>";
        echo "метод ран завершен<br>";
    }


}