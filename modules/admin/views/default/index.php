<?php

/** @var array $tasks */
use yii\helpers\Html;
use yii\helpers\Url;
use yii\jui\Accordion;
use yii\bootstrap\Nav;

$this->title = Yii::t('app', 'Admin panel');
$this->params['breadcrumbs'][] = $this->title;
?>



    

<?=
Nav::widget([
    'items' => [
        [
            'label' => Yii::t('app', 'Admin Task'),
            'url' => ['/admin/task'],
            'linkOptions' => null,
        ],
        [
            'label' => Yii::t('app', 'Admin User'),
            'url' => ['/admin/user'],
            'linkOptions' => null,
        ],
        [
            'label' => Yii::t('app', 'Admin lang'),
            'url' => ['/admin/lang'],
            'linkOptions' => null,
        ],
        [
            'label' => Yii::t('app', 'Admin comments'),
            'url' => ['/admin/comment'],
            'linkOptions' => null,
        ],
    ],
    'dropDownCaret' => true,
    'options' => ['class' => 'nav-pills col-xs-12 col-md-4',], // set this to nav-tab to get tab-styled navigation
]);
?>
</div>
