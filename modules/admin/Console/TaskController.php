<?php

namespace app\modules\admin\console;

use yii\helpers\Console;
use yii\console\Controller;
use app\models\Task;

/**
 * 1. Write the console command that will find the tasks with the expiring deadline, and send an alert to the executors.
 * @package app\commands
 */
class TaskController extends Controller {

    public function actionFindOverdue() {
        $taskList = Task::getOverdue();

        echo 'Found ' . count($taskList) . ' tasks ending today';
        //Send message
        foreach ($taskList as $day => $task):
            echo "Message with ID {$task->user_id} send!!!!";
        endforeach;
    }

}
