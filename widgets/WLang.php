<?php
namespace app\widgets;
use app\models\Lang;
use yii\helpers\ArrayHelper;
class WLang extends \yii\bootstrap\Widget
{
    public function init(){}

    public function run() {
        $items = Lang::find()->all();       
        return $this->render('lang\lang_widget', ['items'=>$items
        ]);
    }
}