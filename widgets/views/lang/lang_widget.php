<?php

use yii\helpers\Html;
use yii\bootstrap\Nav;
?>
<div id="lang">
    <?php
    //Названия языков из базы
    $menuSubItems = [];
    foreach ($items as $lang):
        array_push($menuSubItems, ['label' => Yii::t('app', $lang->name), 'url' => array_merge(
                    \Yii::$app->request->get(), [\Yii::$app->controller->route, 'language' => $lang->url]
            )]
        );

    endforeach;
    //делаем красивое меню
    $menuItems = [
        ['label' => Yii::t('app', 'Language'), 'items' => $menuSubItems],
    ];
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    ?>
</div>