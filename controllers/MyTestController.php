<?php

namespace app\controllers;

use \yii\web\Controller;
use yii\dase\model;
use app\models\Test;
/**
 * Description of MyTestController
 *
 * @author MarchenkoDS 2018
 */
class MyTestController extends Controller{

//put your code here
    public function actionIndex() {
        $modelTest = new Test();
        var_dump($modelTest->validate());
        var_dump($modelTest->getErrors());

        return $this->render('/task/one', ['title' => 'ok']);
    }

}
