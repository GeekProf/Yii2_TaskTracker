<?php

namespace app\controllers;

use Yii;
use app\models\Task;
use app\models\UserTask;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TaskController implements the CRUD actions for Task model.
 */
class TaskController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Task models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new UserTask();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Task model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Task model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Task();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing Task model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Task model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['monthtask']);
    }

    /**
     * Finds the Task model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Task the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Task::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    /**
     * Lists all Task models.
     * @return mixed
     */
    public function actionMonthtask() {

        $userId = \Yii::$app->user->getId();
        $calendar = array_fill_keys(range(1, date("t")), []);
        /* 1. ������� ����������� ������ �� �������� ������� ����� �� �����. */
        $cache = \Yii::$app->cache;
        $key = 'Monthtask' . $userId;
        $dependency = new \yii\caching\DbDependency();
        $dependency->sql = "select count(*) from task";
        if ($cache->exists($key)) {
            $taskList = $cache->get($key);
        } else {

            $taskList = Task::getByCurrentMonth($userId);
            $cache->set($key, $taskList, 10, $dependency);
        }

        if ($taskList) {
            foreach ($taskList as $task) {
                $date = \DateTime::createFromFormat("Y-m-d H:i:s", $task->due_date);
                $calendar[$date->format("j")][] = $task;
            }
        }

        return $this->render('month_task', ['calendar' => $calendar]);
    }

    public function actionCreateNew() {
        $model = new Task();

        $model->user_id = \Yii::$app->user->getId();

        //2. �?спользуя механизм событий добавить функционал отправки пользователю оповещения о новой задаче.
        $model->on(Task::EVENT_AFTER_INSERT, function ($e) {
            echo "Message with ID {$e->sender->user_id}send!!!!";
        });

        if ($model->load(\Yii::$app->request->post()) && $model->save()) {
            echo 'ok';
            $this->redirect(['task/monthtask']);
        }
        return $this->render('create_new', ['model' => $model]);
    }

}
