<?php

$vendorDir = dirname(__DIR__);

return array (
  'yiisoft/yii2-swiftmailer' => 
  array (
    'name' => 'yiisoft/yii2-swiftmailer',
    'version' => '2.0.7.0',
    'alias' => 
    array (
      '@yii/swiftmailer' => $vendorDir . '/yiisoft/yii2-swiftmailer',
    ),
  ),
  'yiisoft/yii2-bootstrap' => 
  array (
    'name' => 'yiisoft/yii2-bootstrap',
    'version' => '2.0.8.0',
    'alias' => 
    array (
      '@yii/bootstrap' => $vendorDir . '/yiisoft/yii2-bootstrap/src',
    ),
  ),
  'yiisoft/yii2-debug' => 
  array (
    'name' => 'yiisoft/yii2-debug',
    'version' => '2.0.13.0',
    'alias' => 
    array (
      '@yii/debug' => $vendorDir . '/yiisoft/yii2-debug',
    ),
  ),
  'yiisoft/yii2-gii' => 
  array (
    'name' => 'yiisoft/yii2-gii',
    'version' => '2.0.7.0',
    'alias' => 
    array (
      '@yii/gii' => $vendorDir . '/yiisoft/yii2-gii/src',
    ),
  ),
  'yiisoft/yii2-faker' => 
  array (
    'name' => 'yiisoft/yii2-faker',
    'version' => '2.0.4.0',
    'alias' => 
    array (
      '@yii/faker' => $vendorDir . '/yiisoft/yii2-faker',
    ),
  ),
  'yiisoft/yii2-jui' => 
  array (
    'name' => 'yiisoft/yii2-jui',
    'version' => '2.0.7.0',
    'alias' => 
    array (
      '@yii/jui' => $vendorDir . '/yiisoft/yii2-jui',
    ),
  ),
  'yiisoft/yii2-redis' => 
  array (
    'name' => 'yiisoft/yii2-redis',
    'version' => '2.0.8.0',
    'alias' => 
    array (
      '@yii/redis' => $vendorDir . '/yiisoft/yii2-redis/src',
    ),
  ),
  'yiisoft/yii2-imagine' => 
  array (
    'name' => 'yiisoft/yii2-imagine',
    'version' => '2.1.1.0',
    'alias' => 
    array (
      '@yii/imagine' => $vendorDir . '/yiisoft/yii2-imagine/src',
    ),
  ),
  'codemix/yii2-localeurls' => 
  array (
    'name' => 'codemix/yii2-localeurls',
    'version' => '1.7.0.0',
    'alias' => 
    array (
      '@codemix/localeurls' => $vendorDir . '/codemix/yii2-localeurls',
    ),
  ),
  'spanjeta/comments' => 
  array (
    'name' => 'spanjeta/comments',
    'version' => '2',
    'alias' => 
    array (
      '@spanjeta/comments' => $vendorDir . '/spanjeta/comments/',
    ),
  ),
  'yii2mod/yii2-enum' => 
  array (
    'name' => 'yii2mod/yii2-enum',
    'version' => '1.7.1.0',
    'alias' => 
    array (
      '@yii2mod/enum' => $vendorDir . '/yii2mod/yii2-enum',
    ),
  ),
  'yii2mod/yii2-moderation' => 
  array (
    'name' => 'yii2mod/yii2-moderation',
    'version' => '1.2.0.0',
    'alias' => 
    array (
      '@yii2mod/moderation' => $vendorDir . '/yii2mod/yii2-moderation',
    ),
  ),
  'yii2mod/yii2-editable' => 
  array (
    'name' => 'yii2mod/yii2-editable',
    'version' => '1.5.0.0',
    'alias' => 
    array (
      '@yii2mod/editable' => $vendorDir . '/yii2mod/yii2-editable',
    ),
  ),
  'yii2mod/yii2-behaviors' => 
  array (
    'name' => 'yii2mod/yii2-behaviors',
    'version' => '1.2.0.0',
    'alias' => 
    array (
      '@yii2mod/behaviors' => $vendorDir . '/yii2mod/yii2-behaviors',
    ),
  ),
  'paulzi/yii2-sortable' => 
  array (
    'name' => 'paulzi/yii2-sortable',
    'version' => '1.0.2.0',
    'alias' => 
    array (
      '@paulzi/sortable' => $vendorDir . '/paulzi/yii2-sortable',
    ),
  ),
  'paulzi/yii2-adjacency-list' => 
  array (
    'name' => 'paulzi/yii2-adjacency-list',
    'version' => '2.1.0.0',
    'alias' => 
    array (
      '@paulzi/adjacencyList' => $vendorDir . '/paulzi/yii2-adjacency-list',
    ),
  ),
  'asofter/yii2-imperavi-redactor' => 
  array (
    'name' => 'asofter/yii2-imperavi-redactor',
    'version' => '0.0.3.0',
    'alias' => 
    array (
      '@yii/imperavi' => $vendorDir . '/asofter/yii2-imperavi-redactor/yii/imperavi',
    ),
  ),
  'yii2mod/yii2-comments' => 
  array (
    'name' => 'yii2mod/yii2-comments',
    'version' => '1.9.9.2',
    'alias' => 
    array (
      '@yii2mod/comments' => $vendorDir . '/yii2mod/yii2-comments',
    ),
  ),
);
