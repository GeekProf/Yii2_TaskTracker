<?php

use yii\db\Migration;

/**
 * Class m180613_074528_lang
 */
class m180613_074528_lang extends Migration
{
  public function safeUp()
{
    $tableOptions = null;
    if ($this->db->driverName === 'mysql') {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
    }

    $this->createTable('lang', [
        'id' => $this->primaryKey(),
        'url' => $this->string()->notNull(),
        'local' => $this->string()->notNull(),
        'name' => $this->string()->notNull(),
        'default' => $this->smallInteger()->notNull()->defaultValue(0),
        'date_update' => $this->Integer()->notNull(),
        'date_create' => $this->Integer()->notNull(),
    ], $tableOptions);

    $this->batchInsert('lang', ['url', 'local', 'name', 'default', 'date_update', 'date_create'], [
        ['en', 'en-EN', 'English', 0, time(), time()],
        ['ru', 'ru-RU', 'Russian', 1, time(), time()],
    ]);
}

public function safeDown()
{
    $this->dropTable('lang');
}
}
