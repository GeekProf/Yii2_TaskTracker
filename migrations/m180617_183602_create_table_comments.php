<?php

use yii\db\Migration;

/**
 * Class m180617_183602_create_table_comments
 */
class m180617_183602_create_table_comments extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    { 
        $this->dropTable('comment');
    $tableOptions = null;
    if ($this->db->driverName === 'mysql') {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
    }
         $this->createTable('comment', [
            'id' => $this->primaryKey(),
            'comment' => $this->text()->notNull(),
            'model_type' => $this->string()->notNull(),
            'model_id' => $this->Integer()->notNull(),
            'state_id' => $this->Integer()->notNull(),
            'type_id' => $this->Integer()->notNull(),
            'create_time' =>  $this->dateTime()->notNull(),
            'create_user_id' => $this->Integer()->notNull(),
        ], $tableOptions
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180617_183602_create_table_comments cannot be reverted.\n";
        $this->dropTable('comment');
        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180617_183602_create_table_comments cannot be reverted.\n";

        return false;
    }
    */
}
